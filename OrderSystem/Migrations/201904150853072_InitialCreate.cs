namespace OrderSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OrderModels",
                c => new
                    {
                        OrderId = c.Int(nullable: false, identity: true),
                        ClientId = c.Int(nullable: false),
                        CargoType = c.String(),
                        ContainerType = c.String(),
                        DepartureHarbour = c.String(),
                        ArrivalHarbour = c.String(),
                    })
                .PrimaryKey(t => t.OrderId)
                .ForeignKey("dbo.ClientModels", t => t.ClientId, cascadeDelete: true)
                .Index(t => t.ClientId);
            
            CreateTable(
                "dbo.ClientModels",
                c => new
                    {
                        ClientId = c.Int(nullable: false, identity: true),
                        CompanyName = c.String(),
                        RepresentativeName = c.String(),
                        Address = c.String(),
                        City = c.String(),
                    })
                .PrimaryKey(t => t.ClientId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderModels", "ClientId", "dbo.ClientModels");
            DropIndex("dbo.OrderModels", new[] { "ClientId" });
            DropTable("dbo.ClientModels");
            DropTable("dbo.OrderModels");
        }
    }
}
