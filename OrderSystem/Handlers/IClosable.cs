using System;

namespace OrderSystem.Handlers
{
    public interface IRequireViewIdentification
    {
        Guid ViewId { get; }
    }
}