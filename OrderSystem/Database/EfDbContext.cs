﻿using System.Data.Entity;
using OrderSystem.Model;

namespace OrderSystem.Database
{
    public class EfDbContext : DbContext
    {
        public DbSet<OrderModel> Orders { get; set; }
        public DbSet<ClientModel> Clients { get; set; }
    }
}