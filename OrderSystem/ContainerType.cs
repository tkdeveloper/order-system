namespace OrderSystem
{
    public enum ContainerType
    {
        SMALL,
        MEDIUM,
        LARGE
    }
}