﻿using System.Windows;
using System.Windows.Controls;

namespace OrderSystem
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
        }


        private void MakeOrderBt_OnClick(object sender, RoutedEventArgs e)
        {
            var createOrderWindow = new CreateOrderWindow();
            Content = createOrderWindow;
        }
    }
}