using System;
using System.Windows;
using OrderSystem.Handlers;

namespace OrderSystem.Helpers
{
    public class WindowManager
    {
        public static void CloseWindow(Guid id)
        {
            foreach (Window window in Application.Current.Windows)
            {
                if (window.DataContext is IRequireViewIdentification wId && wId.ViewId.Equals(id))
                {
                    window.Close();
                }
            }
        }
    }
}