using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using OrderSystem.Database;
using OrderSystem.Handlers;
using OrderSystem.Helpers;
using OrderSystem.Model;

namespace OrderSystem.ViewModel
{
    public class ClientsWindowViewModel : INotifyPropertyChanged,
        IRequireViewIdentification
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public Guid ViewId { get; }
        public RelayCommand CloseWindowCommand { get; set; }
        public string SizeOfContactsTb { get; set; }
        public List<ClientModel> LvClients { get; set; }

        public ClientsWindowViewModel()
        {
            ViewId = Guid.NewGuid();
            CloseWindowCommand = new RelayCommand(CloseWindowAndNotify);
        }

        private void CloseWindowAndNotify(object obj)
        {
            WindowManager.CloseWindow(ViewId);
        }

        public void InitClients()
        {
            LvClients = GetListOfClients();
            SizeOfContactsTb = LvClients.Count.ToString();
        }

        private static List<ClientModel> GetListOfClients()
        {
            using (var context = new EfDbContext())
            {
                return context.Clients.Select(c => c).ToList();
            }
        }
    }
}