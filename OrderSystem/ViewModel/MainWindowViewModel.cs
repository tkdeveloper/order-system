using System.ComponentModel;
using System.Windows.Input;
using OrderSystem.Handlers;
using OrderSystem.View;

namespace OrderSystem.ViewModel
{
    public sealed class MainWindowViewModel : INotifyPropertyChanged
    {
        public ICommand CreateOrderWindowClick { get; set; }
        public ICommand OrdersWindowClick { get; set; }
        public ICommand ClientsWindowClick { get; set; }
        public ICommand CloseWindow { get; set; }

        private CreateOrderWindow _createOrderWindow;
        private OrdersWindow _ordersWindow;
        private ClientsWindow _clientsWindow;

        public event PropertyChangedEventHandler PropertyChanged;

        public MainWindowViewModel()
        {
            CreateOrderWindowClick = new RelayCommand(ClickButtonCreateOrderWindow);
            OrdersWindowClick = new RelayCommand(ClickButtonOrdersWindow);
            ClientsWindowClick = new RelayCommand(ClickButtonClientsWindow);
            CloseWindow = new RelayCommand(CloseCreateOrderWindow);
        }
     
        private void ClickButtonCreateOrderWindow(object obj)
        {
            _createOrderWindow = new CreateOrderWindow();
            _createOrderWindow.Show();
        }

        private void ClickButtonOrdersWindow(object obj)
        {
            _ordersWindow = new OrdersWindow();
            _ordersWindow.Show();
        }

        private void ClickButtonClientsWindow(object obj)
        {
            _clientsWindow = new ClientsWindow();
            _clientsWindow.Show();
        }

        private void CloseCreateOrderWindow(object obj)
        {
            _createOrderWindow.Close();
        }
    }
}