using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Documents;
using OrderSystem.Database;
using OrderSystem.Handlers;
using OrderSystem.Helpers;
using OrderSystem.Model;

namespace OrderSystem.ViewModel
{
    public class OrdersWindowViewModel :
        IRequireViewIdentification
    {
        public RelayCommand BackButtonCommand { get; set; }
        public Guid ViewId { set; get; }
        public List<OrderModel> LvOrders { set; get; }
        public string SizeOfOrdersTb { get; set; }

        public OrdersWindowViewModel()
        {
            ViewId = Guid.NewGuid();
            BackButtonCommand = new RelayCommand(CloseCurrentWindow);
        }

        private void CloseCurrentWindow(object obj)
        {
            WindowManager.CloseWindow(ViewId);
        }

        public void InitListOfOrders()
        {
            LvOrders = GetListOfOrders();
            SizeOfOrdersTb = LvOrders.Count.ToString();
        }

        private static List<OrderModel> GetListOfOrders()
        {
            using (var context = new EfDbContext())
            {
                return context.Orders.Select(s => s).ToList();
            }
        }
    }
}