using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using OrderSystem.Database;
using OrderSystem.Handlers;
using OrderSystem.Helpers;
using OrderSystem.Model;

namespace OrderSystem.ViewModel
{
    public class CreateOrderWindowViewModel : IRequireViewIdentification
    {
        public ICommand CreateOrderAndSaveToDatabaseClick { get; set; }
        public RelayCommand CloseWindowCommand { get; set; }
        public string CompanyNameValue { get; set; }
        public string RepresentativeValue { get; set; }
        public string StreetWithNumber { get; set; }
        public string CargoType { get; set; }
        public string City { get; set; }
        public string ContainerTypeSelected { get; set; }
        public string DepartureHarbour { get; set; }
        public string ArrivalHarbour { get; set; }

        public Guid ViewId { get; }

        public CreateOrderWindowViewModel()
        {
            ViewId = Guid.NewGuid();
            CreateOrderAndSaveToDatabaseClick = new RelayCommand(CreateNewOrder);
            CloseWindowCommand = new RelayCommand(CloseWindowAndNotify);
        }

        private void CreateNewOrder(object obj)
        {
            if (CheckIfAnyElementIsNull())
            {
                ShowDialogToAlertNullElement();
                return;
            }

            if (CheckIfDepartureAndArrivalAreSame())
            {
                ShowDialogToAlertSamePlaces();
                return;
            }

            var client = new ClientModel(1, CompanyNameValue, RepresentativeValue, StreetWithNumber,
                City);
            var order = new OrderModel(1, client, CargoType, ContainerTypeSelected, DepartureHarbour,
                ArrivalHarbour);

            SaveOrderToDatabase(order);
        }

        private static void ShowDialogToAlertSamePlaces()
        {
            var titleMessage = (string) Application.Current.FindResource("TitleMessage");
            var descriptionMessage = (string) Application.Current.FindResource("DescriptionHarboursMessage");
            ShowDialog(titleMessage, descriptionMessage);
        }

        private bool CheckIfDepartureAndArrivalAreSame()
        {
            return DepartureHarbour == ArrivalHarbour;
        }

        private static void SaveOrderToDatabase(OrderModel order)
        {
            using (var context = new EfDbContext())
            {
                context.Orders.Add(order);
                context.SaveChanges();
                ShowDialogToInformSavedElement();
            }
        }

        private static void ShowDialogToAlertNullElement()
        {
            var titleMessage = (string) Application.Current.FindResource("TitleMessage");
            var descriptionMessage = (string) Application.Current
                .FindResource("DescriptionAllFieldsMessage");
            ShowDialog(titleMessage, descriptionMessage);
        }

        private static void ShowDialogToInformSavedElement()
        {
            var titleMessage = (string)Application.Current.FindResource("SavedTitleMessage");
            var descriptionMessage = (string)Application.Current
                .FindResource("SavedDescriptionMessage");
            ShowDialog(titleMessage, descriptionMessage);
        }

        private static void ShowDialog(string titleMessage, string descriptionMessage)
        {
            const MessageBoxButton button = MessageBoxButton.OK;
            const MessageBoxImage icon = MessageBoxImage.Warning;
            MessageBox.Show(descriptionMessage, titleMessage, button, icon);
        }

        private bool CheckIfAnyElementIsNull()
        {
            if (CompanyNameValue == null)
                return true;
            if (RepresentativeValue == null)
                return true;
            if (StreetWithNumber == null)
                return true;
            if (CargoType == null)
                return true;
            if (City == null)
                return true;
            return DepartureHarbour == null;
        }

        private void CloseWindowAndNotify(object obj)
        {
            CloseWindow();
        }

        private void CloseWindow()
        {
            WindowManager.CloseWindow(ViewId);
        }
    }
}