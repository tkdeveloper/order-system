﻿using System.Windows;
using OrderSystem.ViewModel;

namespace OrderSystem.View
{
    public partial class MainWindow: Window
    {
        public MainWindow()
        {
            InitializeComponent();
            InitViewModel();
        }

        private void InitViewModel()
        {
            var mainWindowViewModel = new MainWindowViewModel();
            DataContext = mainWindowViewModel;
        }   
    }
}