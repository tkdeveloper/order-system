using System.Collections.Generic;
using Microsoft.Practices.Prism;
using OrderSystem.Model;
using OrderSystem.ViewModel;

namespace OrderSystem.View
{
    public partial class ClientsWindow
    {
        public ClientsWindow()
        {
            InitializeComponent();
            InitViewModel();
        }

        private void InitViewModel()
        {
            var clientsWindowViewModel = new ClientsWindowViewModel();
            clientsWindowViewModel.InitClients();
            DataContext = clientsWindowViewModel;
        }
    }
}