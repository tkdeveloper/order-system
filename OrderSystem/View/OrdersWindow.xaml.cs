using System.Windows;
using OrderSystem.ViewModel;

namespace OrderSystem.View
{
    public partial class OrdersWindow : Window
    {
        public OrdersWindow()
        {
            InitializeComponent();
            InitViewModel();
        }

        private void InitViewModel()
        {
            var ordersWindowViewModel = new OrdersWindowViewModel();
            ordersWindowViewModel.InitListOfOrders();
            DataContext = ordersWindowViewModel;
        }
    }
}