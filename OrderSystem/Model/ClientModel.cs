using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OrderSystem.Model
{
    public class ClientModel
    {
        [Key] public int ClientId { get; set; }
        public string CompanyName { get; set; }
        public string RepresentativeName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public virtual Collection<OrderModel> Orders { get; set; }

        public ClientModel()
        {
        }

        public ClientModel(int? clientId, string companyName, string representativeName, string address, string city)
        {
            if (clientId != null) ClientId = (int) clientId;
            CompanyName = companyName;
            RepresentativeName = representativeName;
            Address = address;
            City = city;
        }
    }
}