using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OrderSystem.Model
{
    public class OrderModel
    {
        [Key] public int OrderId { get; set; }
        [ForeignKey("ClientModel")] public int ClientId { get; set; }
        public string CargoType { get; set; }
        public string ContainerType { get; set; }
        public string DepartureHarbour { get; set; }
        public string ArrivalHarbour { get; set; }
        [Required] public virtual ClientModel ClientModel { get; set; }

        public OrderModel()
        {
        }

        public OrderModel(int? id, ClientModel client, string cargoType, string containerType,
            string departureHarbour, string arrivalsHarbour)
        {
            if (id != null) OrderId = (int) id;
            ClientModel = client;
            CargoType = cargoType;
            ContainerType = containerType;
            DepartureHarbour = departureHarbour;
            ArrivalHarbour = arrivalsHarbour;
        }
    }
}